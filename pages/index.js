import React from 'react'
import Link from 'next/link'
import '../static/styles/application.less'
import { Layout, Menu, Breadcrumb } from 'antd';

const { Header, Content, Footer } = Layout;

const Index = () =>{
  return (
    <Layout>
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
       
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1"><Link href="/reactclicker">React Clicker</Link></Menu.Item>
        <Menu.Item key="2"><Link href="/reacttimer1">React Timer 1</Link></Menu.Item>
        <Menu.Item key="3"><Link href="/reactclock">React Clock</Link></Menu.Item>
        <Menu.Item key="4"><Link href="/reacttimer2">React Timer 2</Link></Menu.Item>
        <Menu.Item key="5"><Link href="/reactcalculator">React Calculator</Link></Menu.Item>
      </Menu>
    </Header>
  </Layout>
  )
}
export default Index;