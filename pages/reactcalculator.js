import React from 'react'
// import '../static/styles/application.less'
import { useState } from 'react'
import { Icon, Row, Col } from 'antd'
import Index from './index';
const reactCalculator = () => {

  const [result, setResult] = useState('');
  const [history, setHistory] = useState([]);
  const [isHistory, setIsHistory] = useState(false);
  function toggleSign(){
    setResult(()=>(parseFloat(result) * (-1)).toString());
  }
  function showHistory() {
    setIsHistory(!isHistory);
  }
  function calculate() {
    var checkResult = '';
    if (result.includes('--')) {
      checkResult = result.replace('--', '+')
    }
    else {
      checkResult = result;
    }
    try {
    
      let register = [];
      
      register.push(checkResult);
     const expresson=register.join('');
    
      setResult((eval(checkResult) || "") + "")
      checkResult=eval(checkResult);
      history.splice(0, 0, { expresson, checkResult })
      
     
    } catch (e) {
      setResult("error")
    }
  };
  function onClick(button) {
    
    if (button === "=") {
      calculate()
    }
    else if (button === "C") {
      reset()
    }
    else if (button === "back") {
      backspace()
    }
    else {
      if(button === "±"){
        toggleSign();
      }
      else{
      setResult(preResult => (preResult + button))
    }

    }
  };
  function reset() {
    setResult("");
  };

  function backspace() {
    setResult(result.slice(0, -1));
  };
  function clearAll() {
    setHistory([]);
    setResult('');
  }
  return (
   <div>
     <Index/>
    <div className="reactCaculator">
      <div className='banner'>
        <div className="bannerclick">
          <Icon type="calculator" theme="filled" style={{ marginTop: '5px', width: '100%', fontSize: '40px', color: 'white' }} />
        </div>
        <div className="bannerReact">
          React Calc
  </div>
      </div>
      <div className="bodyCalculator">
        <div className="resultCalculator" style={{textAlign:'right'}}>

          <span style={{width:'100%',wordBreak:'break-all'}}>{result}</span>
        </div>
        <div className="keyboard">
          <Row>
            <Col span={24}><button><Icon type="loading-3-quarters" onClick={showHistory} /></button></Col>
          </Row>
          {!isHistory ? 
          <>
            <Row>
              <Col span={6}><button style={{ color: '#0087ff' }} onClick={clearAll}>CE</button></Col>
              <Col span={6}><button name="C" onClick={e => onClick(e.target.name)} style={{ color: '#0087ff' }} >C</button></Col>
              <Col span={6}><button name="back" onClick={e => onClick(e.target.name)} style={{ color: '#0087ff' }} >&#8882;</button></Col>
              <Col span={6}><button name="/" onClick={e => onClick(e.target.name)} style={{ color: '#0087ff' }}>&#8785;</button></Col>
            </Row>
            <Row>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="7" onClick={e => onClick(e.target.name)}>7</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="8" onClick={e => onClick(e.target.name)}>8</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="9" onClick={e => onClick(e.target.name)}>9</button></Col>
              <Col span={6}><button style={{ color: '#0087ff' }} name="*" onClick={e => onClick(e.target.name)}>X</button></Col>
            </Row>
            <Row>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="4" onClick={e => onClick(e.target.name)}>4</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="5" onClick={e => onClick(e.target.name)}>5</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="6" onClick={e => onClick(e.target.name)}>6</button></Col>
              <Col span={6}><button style={{ color: '#0087ff' }} name="-" onClick={e => onClick(e.target.name)}>-</button></Col>
            </Row>
            <Row>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="1" onClick={e => onClick(e.target.name)}>1</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="2" onClick={e => onClick(e.target.name)}>2</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="3" onClick={e => onClick(e.target.name)}>3</button></Col>
              <Col span={6}><button style={{ color: '#0087ff' }} name="+" onClick={e => onClick(e.target.name)}>+</button></Col>
            </Row>
            <Row>
              <Col span={6}><button style={{ color: '#0087ff' }} name="±"onClick={e => onClick(e.target.name)}>±</button></Col>
              <Col span={6}><button style={{ fontWeight: 'bold' }} name="0" onClick={e => onClick(e.target.name)}>0</button></Col>
              <Col span={6}><button style={{ color: '#0087ff' }} name="." onClick={e => onClick(e.target.name)}>.</button></Col>
              <Col span={6}><button style={{ color: '#0087ff' }} name="=" onClick={e => onClick(e.target.name)}>=</button></Col>
            </Row> 
            </>
            : <div style={{height:'135px',overflow:'scroll'}}
            
            ><Icon type="delete" onClick={clearAll}/>{history.map((value, index) => {

              return <div key={value.index} style={{height:'auto',width:'100%',textAlign:'right'}}>
                <h4 style={{height:'10px'}}>{value.expresson}=</h4>
                <h4 style={{height:'10px'}}>{value.checkResult}</h4>
              </div>
              
            })}
            </div>
            }
      
        </div>

      
      </div>
      <style jsx>
        {`
             .reactCaculator{
                width:200px;
                height:500px;
                margin:70px auto;
                
              }
              .banner div{
                display:inline-block;
              }
              .banner{
                width:100%;
                background-color:#343a40;
                height:50px;
                position:relative;
              }
              .bannerclick{
                width:100px;
                height:100%;
              }
              .bannerReact{
                color:white;
                position:absolute;
                top:15px;
                left:80px;
              }
              .bannerclick svg{
                width:100px!important;
                height:100px!important;
              }
              .bodyCalculator{
                position:relative;
                width:100%;
                height:300px;
              }
              .keyboard{
                background-color:rgba(245, 34, 45, 0.15);
                text-align:center;
                width:100%;
                height:auto;
                position:absolute;
                bottom:0px;
              }
              .keyboard button {
                width:100%;
              }
            `}

      </style>
    </div>
    </div>
  )
}
export default reactCalculator;