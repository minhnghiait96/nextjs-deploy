// assignment 2
import React from 'react'
import { Icon } from 'antd';
import { useState } from 'react';
import Index from './index';
const reactclicker = () => {
  const [count, setCount] = useState(0);
  return(
   <div>
     <Index/>
  <div className="reactClicker">
    
    <div className='banner'>
      <div className="bannerclick">
        <Icon type="like" theme="filled" style={{width:'100%',fontSize: '40px', color: '#08c' }} />
      </div>
      <div className="bannerReact">
        React Clicker
      </div>
   
    </div>
    <div className="screenShow">
    <div className="count" style={{marginTop:'100px'}}>
      <h1 style={{fontSize:'50px'}}>{count}</h1>
    </div>
    <div className='row'>

      <div className="plus"><Icon type="plus" onClick={()=>setCount(count+1)} style={{ fontSize:'30px',backgroundColor: 'green',marginLeft:'16px',color:'white'}}/></div>
      <div className="sync"><Icon type="sync" onClick={()=>setCount(0)} style={{ fontSize:'30px',backgroundColor: 'yellow',marginLeft:'16px'}}/></div>
     <div className="minus"><Icon type="minus"  onClick={()=>setCount(count-1)} style={{ fontSize:'30px',backgroundColor: 'pink' ,marginLeft:'16px',color:'white'}}/></div>
     
    </div>
    </div>
    <style jsx>{`
     .reactClicker{
       width:200px;
       height:500px;
       margin:70px auto;
       
     }
     .banner div{
       display:inline-block;
     }
     .banner{
       width:100%;
       background-color:#343a40;
       height:50px;
       position:relative;
     }
     .bannerclick{
       width:100px;
       height:100%;
     }
     .bannerReact{
       color:white;
       position:absolute;
       top:15px;
       left:80px;
     }
     .bannerclick svg{
       width:100px!important;
       height:100px!important;
     }
     .screenShow{
       border:3px solid gray;
     }
    button{
      width:100%;
    }
     .row div{
      width:33%;
      height:33%;
       display:inline-block;
    
     }
     .plus{
       background-color:green;
     }
     .sync{
       background-color:yellow;
     }
     .minus{
       background-color:pink;
     }
     .count{
       text-align:center;
       height:200px;
     }
     .plusName{
      backgcolor:yellow;
     }
    `}</style>
  </div>
  </div>
  )
}

export default reactclicker;