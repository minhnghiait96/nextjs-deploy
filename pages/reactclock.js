
import React from 'react'
import { Icon } from 'antd';
import { useState } from 'react';
import Switch from '@material-ui/core/Switch';
import '../static/styles/application.less'
import Index from './index';
const reactclock = () => {
    const monthNames=[
        'January', 'February', 'March',
        'April', 'May', 'June',
        'July', 'August', 'September',
        'October', 'November', 'December'
    ];
    const daysName=[
        'Sun', 'Mon', 'Tues', 'Wed',
        'Thu', 'Fri', 'Sat'
    ];
    const FormatDay = (date) =>{
        let dayofweek = daysName[date.getDay()];
        let day = formatUnitOfTime(date.getDate());
        let month=monthNames[date.getMonth()];
        let years = date.getFullYear();
        return `${dayofweek} ${day} ${month} ${years}`;
    }
    const formatUnitOfTime =(unitOfTime)=> {
        return unitOfTime < 10 ? `0${unitOfTime}` : `${unitOfTime}`;
    }
    const FormatTime = (date) =>{
        let hours = formatUnitOfTime(date.getHours());
        let minutes = formatUnitOfTime(date.getMinutes());
        let seconds = formatUnitOfTime(date.getSeconds());
        return `${hours}:${minutes}:${seconds}`
    }
  const [times, setTime] = useState(FormatTime(new Date()));
  const [day,setDay] = useState(FormatDay(new Date()));
  const [isChecked,setChecked] = useState(true)
  const onChange =() =>{
    setChecked(!isChecked);
  }
  setInterval(function(){
    setTime(times => FormatTime(new Date()))
  },1000)
 
  return (
    <div>
      <Index/>
    <div className="reactclock">
      
      <div className='banner'>
        <div className="bannerclick">
         
          <Icon type="clock-circle" theme="filled" style={{marginTop:'5px',width: '100%', fontSize: '40px', color: 'white' }}/>
        </div>
        <div className="bannerReact">
          React Clock
      </div>
   
      </div>
      <div className="showIcon">
        <div style={{width:'50%'}} className="switchtoggle">
      <Switch 
        checked={isChecked}
        onChange={onChange}
        value={isChecked}
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      />
      </div>
      <div style={{width:'50%',paddingTop:'10px'}} className="calender">
        <Icon type="calendar" style={{fontSize:'20px'}}/>
        </div>
        </div>
      <div className="circle">
     
       <div className="showTime">
          <p style={{fontSize:'35px',height:'20px'}}>{times}</p>
          {isChecked?
          <p style={{fontSize:'25px',height:'20px'}}>{day}</p>:
          <br/>
        }
       </div>
      </div>

      <style jsx>{`
     .reactclock{
       width:200px;
       height:500px;
       margin:70px auto;
     }
     .banner div{
       display:inline-block;
     }
     .banner{
       width:100%;
       background-color:#343a40;
       height:50px;
       position:relative;
     }
     .bannerclick{
       width:100px;
       height:100%;
     }
     .bannerReact{
       color:white;
       position:absolute;
       top:15px;
       left:80px;
     }
     .bannerclick svg{
       width:100px!important;
       height:100px!important;
     }
     .showIcon{
       position:relative;
     }
     .showIcon div{
       display:inline-block;
       width:100%;
       height:50px;
     }
     .calender{
       position:absolute;
       top:0px;
        left:100px;
     }
     .switchtoggle{
       position:absolute;
       top:2px;
       left:50px;
     }
     .circle {
        position:relative;
       background-color:#282c34;
       border:5px solid #e5e5e5;    
       height:200px;
      border-radius:50%;
      -moz-border-radius:50%;
      -webkit-border-radius:50%;
       width:200px;
      margin:auto;
      margin-top:50px;
      overflow:hidden;
  }
  .showTime{
    color:#4a8a9d;
    display:block;
    text-align:center;
    position: absolute;
    top: 50%;
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
  height:auto;
  width:100%;
  }
    `}</style>
    </div>
    </div>)
}

export default reactclock;