import React from 'react'

import { useState } from 'react';
import Index from './index';
import { Icon, Row, Col } from 'antd'
const reacttimer1 = () => {
  const [seconds, setClock] = useState(0);
  const [timerId, setTimerId] = useState(null);

  const startIncrement = () => {
    if (!timerId) {
      setTimerId(setInterval(function () {
        setClock(preSecond => (preSecond + 1));
      }, 1000));
    } else {
      clearInterval(timerId);
      setTimerId(null);
    }
  }
  return (
    <div>
      <Index/>
    <div className="reacttimer1">
      <div className='banner'>
        <div className="bannerclick">
          <Icon type="hourglass" theme="filled" style={{ width: '100%', fontSize: '40px', color: 'white' }} />
        </div>
        <div className="bannerReact">
          React Timer
      </div>
     
      </div>
     
      <div className="circle">
        <div className="showClock" style={{ marginTop: '35%', textAlign: 'center' }}>
          <h1>{seconds}</h1>
        </div>
        <div className="startSync">
        <Row>
        {!timerId ? <Col span={12} style={{backgroundColor:'#6abe7d',height:'50px'}}><Icon type="caret-right"  style={{ marginLeft:'15px',width: '100%', fontSize: '40px', color: 'white' }} onClick={() => startIncrement()}/>:</Col>:<Col span={12} style={{backgroundColor:'#6abe7d',height:'50px',}}><Icon type="pause" style={{ width: '100%', fontSize: '40px', color: 'white' ,marginLeft:'15px'}} onClick={() => startIncrement()}/></Col> }
      <Col span={12} style={{backgroundColor:'#0778fd', height: '50px'}}><Icon type="redo"  style={{ marginTop:'5px',width: '70%', fontSize: '30px', color: 'white'}} onClick={()=>setClock(0)}/></Col>
    </Row>
         
        </div>
      </div>

      <style jsx>{`
     .reacttimer1{
       width:200px;
       height:500px;
       margin:70px auto;
       
     }
     .banner div{
       display:inline-block;
     }
     .banner{
       width:100%;
       background-color:#343a40;
       height:50px;
       position:relative;
     }
     .bannerclick{
       width:100px;
       height:100%;
     }
     .bannerReact{
       color:white;
       position:absolute;
       top:15px;
       left:80px;
     }
     .bannerclick svg{
       width:100px!important;
       height:100px!important;
     }
     .circle {
        position:relative;
       background-color:#fff;
       border:5px solid #e5e5e5;    
       height:200px;
      border-radius:50%;
      -moz-border-radius:50%;
      -webkit-border-radius:50%;
       width:200px;
      margin:auto;
      
      margin-top:50px;
      overflow:hidden;
  }
  .startSync{
   
    width:100%;
    height:50px;
    position:absolute;
    bottom:0px;

    
  }
  .startSync div {
    display:inline-block;
    width:100%
    height:100%
  }
  .startSync .start{
    position:relative;
    width:50%;
    background-color:#6abe7d;
}
.startSync .async{
  width:50%;
  background-color:#0778fd;
}
.icontest{
  position:absolute;
  transform:translateY(-50%);
  top:30%;
  left:30%;
}
    `}</style>
    </div>
    </div>)
}
export default reacttimer1;