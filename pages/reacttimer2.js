

import React, { useState } from 'react'
import { Row, Col, Icon,Button } from 'antd';
import '../static/styles/application.less'
import Index from './index';
const reacttimer2 = () => {
    const [hours, setHour] = useState('00');
    const [minutes, setMinute] = useState('00');
    const [seconds, setSecond] = useState('00')
    const [unitoftimes, setUnitOfTimes] = useState(null);
    const [status,setStatus] = useState(null);
    const [timeInterval,setTimeInterval]=useState(null);
    const [timerId,setTimeId] = useState(null);
    const [isShow,setIsShow] = useState(true);
    const [isShowStop,setIsShowStop]=useState(true);
    const [isStart,setIsStart] = useState(null)
    function canStart (){
        setIsStart((prevState)=>(
            status!=='STARTED' && parseInt(hours)>0 || parseInt(minutes)>0 || parseInt(seconds)>0
        )
        )
    }
    function handleUnitOfTIme(unitoftime) {
      
        setUnitOfTimes(unitoftime)
    }
    function handleStop(){
        setIsShowStop(!isShowStop);
        if(status==='STARTED'){
            clearInterval(timerId);
            setStatus('STOPPED')
        }
    }
    function getTime(timeInMilliseconds) {

        let time = timeInMilliseconds;
        const hours = formatUnitOfTime(Math.floor(time / (60 * 60 * 1000)));
        time = time % (60 * 60 * 1000);
        const minutes = formatUnitOfTime(Math.floor(time / (60 * 1000)));
        time = time % (60 * 1000);
        const seconds = formatUnitOfTime(Math.floor(time / 1000));
        const milliseconds = formatUnitOfTime(time % 1000);
        if(time>0){
            return `${hours}:${minutes}:${seconds}:${milliseconds}`;
        }
        else{
            return `00:00:00:00`
        }
    

    }

    function formatUnitOfTime(unitOfTime) {
        return unitOfTime < 10
            ? `0${unitOfTime}`.substring(0, 2)
            : unitOfTime.toString().substring(0, 2);
    }
    function handleStart(){
        setIsShow(false)
        if(status !=='STARTED'){
            setStatus('STARTED');
            const totalMiliseconds =((parseInt(hours)*60*60)+(parseInt(minutes)*60)+(parseInt(seconds)))*1000;
            setTimeInterval(parseInt(totalMiliseconds));
            setTimeId(setInterval(()=>{
                setTimeInterval(prevState=>{
                    if(prevState===0){
                        setStatus(null);
                        return clearInterval(timerId);
                       
                    } 
                    return (prevState - 10)
                });        
            },10));
        }
    }
    function hanldeResume(){
        setIsShowStop(!isShowStop);
        if(status ==='STOPPED'){
            setStatus('STARTED');
            setTimeId(setInterval(()=>{
                setTimeInterval(prevState=>{
                    if(prevState===0){
                        clearInterval(timerId);
                        setStatus(null);
                    }
                    return (prevState - 10);
                });
            },10));
        }
    }
    function handleReset(){
        clearInterval(timerId);
        setIsShowStop(true);
         setTimeInterval(null);
        setIsShow(true);
        setStatus(null);
    }
    function getValue(time) {
        if (unitoftimes === 'HH') {
            setHours(time);
        } else if ((unitoftimes === 'MM')) {
            setMinutes(time);
        } else if ((unitoftimes === 'SS')) {
            setSeconds(time);
        }
        canStart();
    }
    function setHours(hours) {
        if (hours < 0) {
            setHour('00')
        } else {
            setHour(prevState => {
                hours = parseInt(formatTime(prevState + hours));
                if (hours > 99) {
                    hours = prevState;
                }
                return formatTime(hours);
            });

        }
    }
    function setMinutes(minutes) {
        if (minutes < 0) {
            setMinute('00')
        } else {
            setMinute(prevState => {
                minutes = parseInt(formatTime(prevState + minutes));
                 if (minutes > 59) {
                    minutes = parseInt(minutes.toString().slice(minutes.toString().length - 1));
                }
                return formatTime(minutes);
            }
            );
        }
        // }
    }
    function setSeconds(seconds) {
        if (seconds < 0) {
            setSecond('00')
        } else {
            setSecond((prevState)=>{
                seconds = parseInt(formatTime(prevState+seconds));
                if(seconds<60){
                    if(parseInt(seconds.toString()[0])>5){
                        seconds=59;
                    }
                 } else if (seconds >59){
                        seconds = parseInt(seconds.toString().slice(seconds.toString().length-1));
                    }
                    return formatTime(seconds);
                
            });
         
        }
    }
    function formatTime(time) {
        time = parseInt(time);
        return time < 10 ? '0' + time : time.toString().slice(time.toString().length - 2);
    }
   
    

    return (
        <div>
            <Index/>
        <div className="reacttimer2">
            <div className='banner'>
                <div className="bannerclick">
                    <Icon type="hourglass" theme="filled" style={{ width: '100%', fontSize: '40px', color: 'white' }} />
                </div>
                <div className="bannerReact">
                    React Timer
      </div>
            </div>
            {isShow ?
            <div className="bodyReacttimer">
                <div className="showTime">
                    <Row>
                        <Col span={8}>H</Col>
                        <Col span={8}>M</Col>
                        <Col span={8}>S</Col>

                    </Row>
                    <Row>
                        <Col span={8}><input type="text" maxLength="2" value={hours} onFocus={() => handleUnitOfTIme('HH')} style={{textAlign:'center'}} /></Col>
                        <Col span={8}><input type="text" maxLength="2" value={minutes} onFocus={() => handleUnitOfTIme('MM')} style={{textAlign:'center'}} /></Col>
                        <Col span={8}><input type="text" maxLength="2" value={seconds} onFocus={() => handleUnitOfTIme('SS')} style={{textAlign:'center'}}/></Col>

                    </Row>
                </div>
                <div className="inputNumber">
                    <Row>
                        <Col span={8}><input type="button" value="1" onClick={() => getValue(1)} /></Col>
                        <Col span={8}><input type="button" value="2" onClick={() => getValue(2)} /></Col>
                        <Col span={8}><input type="button" value="3" onClick={() => getValue(3)} /></Col>

                    </Row>
                    <Row>
                        <Col span={8}><input type="button" value="4" onClick={() => getValue(4)} /></Col>
                        <Col span={8}><input type="button" value="5" onClick={() => getValue(5)} /></Col>
                        <Col span={8}><input type="button" value="6" onClick={() => getValue(6)} /></Col>

                    </Row>
                    <Row>
                        <Col span={8}><input type="button" value="7" onClick={() => getValue(7)} /></Col>
                        <Col span={8}><input type="button" value="8" onClick={() => getValue(8)} /></Col>
                        <Col span={8}><input type="button" value="9" onClick={() => getValue(9)} /></Col>

                    </Row>
                    <Row>
                        <Col span={8}><input type="button" value="*" onClick={() => getValue(-1)} /></Col>
                        <Col span={8}><input type="button" value="0" onClick={() => getValue(0)} /></Col>
                        <Col span={8}><button onClick={() => getValue('')}>&nbsp;</button></Col>

                    </Row>
                 
                </div>
                <Button onClick={handleStart} disabled={!isStart} value="Start" style={{marginTop:'50px',position:'absolute',bottom:'0px',width:'100%',backgroundColor:'green',border:'none'}}>Started</Button>
                {/* type="button" /> */}
            </div>:<div className="showTimeStop">
                <div className="showTimer">
                <Row>
                        <Col span={6}>H</Col>
                        <Col span={6}>M</Col>
                        <Col span={6}>S</Col>
                        <Col span={6}>MS</Col>
                    </Row>
                    <Row>
                        
                    <h1 style={{color:'#70aff4'}}>{getTime(timeInterval)}</h1>
                    </Row>
                </div>
                <div className="stopReset">
                <Row>
                        {isShowStop ? <Col span={12}><input type="button" value="STOP" style={{backgroundColor:'#ae4954',border:'none'}} onClick={handleStop}/></Col>:<Col span={12}><input type="button" value="RESUME" style={{backgroundColor:'green',border:'none'}} onClick={hanldeResume}/></Col>}
                        <Col span={12}><input type="button" value="RESET" onClick={handleReset} style={{backgroundColor:'#3476c2',border:'none'}}/></Col>
                    </Row>
                </div>
            </div>
            }
           
           
            
            <style jsx>{`
     .reacttimer2{
       width:200px;
       height:500px;
       margin:70px auto;
     }
     .banner div{
       display:inline-block;
     }
     .banner{
       width:100%;
       background-color:#343a40;
       height:50px;
       position:relative;
     }
     .bannerclick{
       width:100px;
       height:100%;
     }
     .bannerReact{
       color:white;
       position:absolute;
       top:15px;
       left:80px;
     }
     .bannerclick svg{
       width:100px!important;
       height:100px!important;
     }
     .bodyReacttimer{
         position:relative;
         width:160px;
         height:300px;
         margin:auto;
         border:5px solid #A3A3A3;
         margin-top:50px;
     }
     .inputNumber{
         position:absolute;
         bottom:30px;
         width:100%;
         heigth:auto;
        
         border:5px solid #777777;
     }
    .bodyReacttimer .inputStart {
        backgo
        position:absolute;
        bottom:0px;
        width: 100%;
        color:white;
        background-color:#39a150;
        border:none;
    }
    .showTime{
        margin-top:30px;
        text-align:center;
        
    }
    input[type=text]{
        text-algin:center;
        width:100%;
    }
    button{
        width:100%;
    }
    .inputNumber input[type=button]{
        width:100%;
    }
    .showTimeStop{
        
        height:300px;
        width:160px;
        position:relative;
        margin:auto;
        margin-top:50px;
        border:5px solid #A3A3A3;  
    }
    .showTimer{
        margin-top:80px;
        text-align:center;
    }
    .stopReset{
        position:absolute;
        bottom:0px;
        width:100%;
        height:23px;
        background-color:blue;
    }
    .stopReset input[type=button]{
        width:100%;
    }
    `}</style>
        </div>
        </div>)
}


export default reacttimer2;